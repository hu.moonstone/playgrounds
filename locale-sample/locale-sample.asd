(asdf:defsystem :locale-sample
  :serial t
  :pathname "src/lisp"
  :components ((:file "package")
               (:file "locale-sample"))
  :depends-on (:asdf :alexandria :gettext))
