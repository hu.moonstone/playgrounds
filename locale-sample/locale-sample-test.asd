(asdf:defsystem :locale-sample-test
  :serial t
  :pathname "test/lisp"
  :components ((:file "package")
               (:file "locale-sample-test"))
  :depends-on (:asdf :fiveam :locale-sample))
