(in-package :locale-sample)

(setup-gettext :locale-sample "locale-sample")
(preload-catalogs #.(asdf:system-relative-pathname :locale-sample "resources/locale/"))

(defun replace-all (string part replacement &key (test #'char=))
    (with-output-to-string (out)
      (loop with part-length = (length part)
            for old-pos = 0 then (+ pos part-length)
            for pos = (search part string
                              :start2 old-pos
                              :test test)
            do (write-string string out
                             :start old-pos
                             :end (or pos (length string)))
            when pos do (write-string replacement out)
         while pos)))

(defparameter *labels* (make-hash-table))
(dolist (label `(,(_ "Dog")
                  ,(_ "Cat")
                  ,(_ "Penguin")))

  (let ((key (alexandria:make-keyword (string-upcase (replace-all label " " "-")))))
    (setf (gethash key *labels*) label)))


(defun main ()
  (setf *current-locale* "ja")
  (format t "----------------------------~%")
  (format t "~A~%" (_ (gethash :dog *labels*)))
  (format t "~A~%" (_ (gethash :cat *labels*)))
  (format t "~A~%" (_ (gethash :penguin *labels*))))
