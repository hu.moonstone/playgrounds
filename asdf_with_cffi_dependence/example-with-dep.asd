(asdf:defsystem #:example-with-dep
    :serial t
    :depends-on (#:asdf #:cffi)
    :components ((:file "package")
                 (:file "example")))
