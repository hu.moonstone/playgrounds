(in-package #:example)

(pushnew (merge-pathnames #P"lib/" *default-pathname-defaults*) cffi:*foreign-library-directories*)
(cffi:define-foreign-library mylib
    (t (:default "mylib")))
(cffi:load-foreign-library 'mylib)
(defun test-function ()
   (print (cffi:foreign-funcall "normal_plus" :int 10 :int 20 :int)))
