(asdf:defsystem #:example-with-dep
    :serial t
    :depends-on (#:ltk)
    :components ((:file "package")
                 (:file "example")))
